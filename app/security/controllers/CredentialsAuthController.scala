package security.controllers

import java.security.ProviderException
import javax.inject.Inject

import com.mohiva.play.silhouette.api.exceptions.{ConfigurationException}
import com.mohiva.play.silhouette.api.{Environment, LoginEvent, Silhouette}
import com.mohiva.play.silhouette.api.services.AuthInfoService
import com.mohiva.play.silhouette.api.util.Credentials
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator
import com.mohiva.play.silhouette.impl.exceptions.IdentityNotFoundException
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import user.services.UserQueriesService
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.Json
import play.api.mvc.Action
import user.model.User

import scala.concurrent.Future
import security.json.SignIn

/**
 * The credentials auth controller.
 *
 * @param env The Silhouette environment.
 */
class CredentialsAuthController @Inject() (
                                            implicit val env: Environment[User, JWTAuthenticator],
                                            val userQueriesService: UserQueriesService,
                                            val authInfoService: AuthInfoService)
  extends Silhouette[User, JWTAuthenticator] {

  import security.json.Format._
  import user.json.Format._

  /**
    * Authenticates a user against the credentials provider.
    *
    * receive json like this:
    * {
    * 	  "identifier": "...",
    *  	"password": "..."
    * }
    *
    * @return The result to display.
    */
  def authenticate = Action.async(parse.json[SignIn]) { implicit request =>
    val signIn:SignIn = request.body
    (env.providers.get(CredentialsProvider.ID) match {
      case Some(p: CredentialsProvider) => p.authenticate(Credentials(signIn.email, signIn.password))
      case _                            => Future.failed(new ConfigurationException(s"Cannot find credentials provider"))
    }).flatMap { loginInfo =>
      userQueriesService.retrieve(loginInfo).flatMap {
        case Some(user) => env.authenticatorService.create(user.loginInfo).flatMap { authenticator =>
          env.eventBus.publish(LoginEvent(user, request, request2lang))
          env.authenticatorService.init(authenticator).flatMap { token =>
            env.authenticatorService.embed(token, Future.successful {
              Ok(Json.toJson(Json.obj("token" -> token)))
            })
          }
        }
        case None =>
          Future.failed(new IdentityNotFoundException("Couldn't find user"))
      }
    }.recover {
      case e: ConfigurationException =>
        InternalServerError(Json.obj("message" -> "Misconfiguration error: check an authentication provider is configured"))
      case e: Exception =>
        Unauthorized(Json.toJson("Invalid credentials: " + e.getMessage))
    }//recoverWith(exceptionHandler)
  }

  def getAutenticatedUser() = SecuredAction.async { implicit request =>
    Future.successful(Ok(Json.toJson(request.identity)))
  }

}
