package security.json

import com.mohiva.play.silhouette.api.util.Credentials
import play.api.libs.json._

object Format {

  implicit val credentialsWrites = Json.writes[Credentials]
  implicit val credentialseads = Json.reads[Credentials]

  implicit val signUpWrites = Json.writes[SignUp]
  implicit val signUpReads = Json.reads[SignUp]

  implicit val signInWrites = Json.writes[SignIn]
  implicit val signInReads = Json.reads[SignIn]

}
