package user.persistence

import java.util.UUID
import com.mohiva.play.silhouette.api.LoginInfo
import sharedkernel.common.persistence.Tables.{LoginInfoRow, LoginInfoTable, UserLoginInfoRow, UserLoginInfoTable, UsersRow, UsersTable}
import play.Logger
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick._
import user.model.User

import scala.concurrent.Future

/**
 * Give access to the user object using Slick
 */
class UserDAOImpl extends UserDAO {

  import play.api.Play.current

  /**
   * Finds a user by its login info.
   *
   * @param loginInfo The login info of the user to find.
   * @return The found user or None if no user for the given login info could be found.
   */
  def find(loginInfo: LoginInfo) = {
    DB withSession { implicit session =>
      Future.successful {
        LoginInfoTable.filter(
          x => x.providerId === loginInfo.providerID && x.providerKey === loginInfo.providerKey
        ).firstOption match {
          case Some(info) =>
            UserLoginInfoTable.filter(_.loginInfoId === info.id).firstOption match {
              case Some(userLoginInfo) =>
                UsersTable.filter(_.id === userLoginInfo.userId).firstOption match {
                  case Some(user) =>
                    Some(User(UUID.fromString(user.id), loginInfo, user.firstName, user.lastName, user.fullName, user.email, user.avatarUrl))
                  case None => None
                }
              case None => None
            }
          case None => None
        }
      }
    }
  }

  /**
   * Finds a user by its user ID.
   *
   * @param userID The ID of the user to find.
   * @return The found user or None if no user for the given ID could be found.
   */
  def find(userID: UUID) = {
    DB withSession { implicit session =>
      Future.successful {
        UsersTable.filter(
          _.id === userID.toString
        ).firstOption match {
          case Some(user) =>
            UserLoginInfoTable.filter(_.userId === user.id).firstOption match {
              case Some(info) =>
                LoginInfoTable.filter(_.id === info.loginInfoId).firstOption match {
                  case Some(loginInfo) =>
                    Some(User(UUID.fromString(user.id), LoginInfo(loginInfo.providerId, loginInfo.providerKey), user.firstName, user.lastName, user.fullName, user.email, user.avatarUrl))
                  case None => None
                }
              case None => None
            }
          case None => None
        }
      }
    }
  }

  /**
   * Saves a user.
   *
   * @param user The user to save.
   * @return The saved user.
   */
  def save(user: User) = {
    DB withSession { implicit session =>
      Future.successful {
        val dbUser = UsersRow(user.userID.toString, user.firstName, user.lastName, user.fullName, user.email, user.avatarURL)
        UsersTable.filter(_.id === dbUser.id).firstOption match {
          case Some(userFound) => UsersTable.filter(_.id === dbUser.id).update(dbUser)
          case None => UsersTable.insert(dbUser)
        }
        var dbLoginInfo = LoginInfoRow(0, user.loginInfo.providerID, user.loginInfo.providerKey)
        // Insert if it does not exist yet
        LoginInfoTable.filter(info => info.providerId === dbLoginInfo.providerId && info.providerKey === dbLoginInfo.providerKey).firstOption match {
          case None => LoginInfoTable.insert(dbLoginInfo)
          case Some(info) => Logger.debug("Nothing to insert since info already exists: " + info)
        }
        dbLoginInfo = LoginInfoTable.filter(info => info.providerId === dbLoginInfo.providerId && info.providerKey === dbLoginInfo.providerKey).first
        // Now make sure they are connected
        UserLoginInfoTable.filter(info => info.userId === dbUser.id && info.loginInfoId === dbLoginInfo.id).firstOption match {
          case Some(info) =>
            // They are connected already, we could as well omit this case ;)
          case None =>
            UserLoginInfoTable += UserLoginInfoRow(dbUser.id, dbLoginInfo.id)
        }
        user // We do not change the user => return it
      }
    }
  }
}
