package user.services

import com.mohiva.play.silhouette.api.services.IdentityService
import user.model.User

/**
 * Handles query actions to users.
 */
trait UserQueriesService extends IdentityService[User]
