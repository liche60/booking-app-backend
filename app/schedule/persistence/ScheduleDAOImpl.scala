package schedule.persistence

import javax.inject.Inject

import play.Logger
import play.api.db.slick._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import schedule.json.ScheduleBasicQuery
import schedule.model.{Schedule, ScheduleStatus}
import schedule.route.persistence.RouteDAO
import sharedkernel.common.persistence.Tables.{SchedulesRow, SchedulesTable}
import sharedkernel.common.persistence.Tables.profile.simple._
import com.github.tototoshi.slick.JdbcJodaSupport._
import org.joda.time.DateTime
import schedule.route.model.Route

import play.api.Play.current

/**
 * Give access to the user object using Slick
 */
class ScheduleDAOImpl  @Inject()(routeDAO: RouteDAO) extends ScheduleDAO {

  def retrieve(basicQuery: ScheduleBasicQuery): List[Schedule] =
    DB withSession { implicit session =>
      val routes = routeDAO.find(basicQuery.srcAirport, basicQuery.dstAirport)
      val routesId = routes.map(_.rid)
      var query = SchedulesTable.filter(row => row.rid inSet routesId)
      if(basicQuery.departDate.isDefined)
        query = query.filter(row =>
          row.departDate >= basicQuery.departDate.get &&
          row.departDate < basicQuery.departDate.get.plusDays(1))
      if(basicQuery.orderByFare)
        query = query.sortBy(_.fare.asc)
      else
        query = query.sortBy(_.departDate.asc)
      query
        .mapResult(row => buildSchedule(row, routes.filter(_.rid == row.rid).head))
        .list
    }

  def updateScheduleSeatsAndStatus(scheduleId: Int, bookedSeats: Int, status: ScheduleStatus.Value): Int =
    DB.withSession { implicit s =>
      SchedulesTable.filter(_.sid === scheduleId)
        .map(schedule => (schedule.bookedSeats, schedule.status))
        .update(bookedSeats, status)
    }

  /** Used to create mock schedules */

  def list: List[Schedule] =
    DB withSession { implicit session =>
      val routes = routeDAO.list()
      SchedulesTable
        .mapResult(row => buildSchedule(row, routes.filter(_.rid == row.rid).head))
        .list
    }

  def create(schedule: Schedule): Schedule =
    DB.withSession { implicit s =>
      val row = SchedulesRow(
        0,
        schedule.route.rid,
        schedule.departDate,
        schedule.status,
        schedule.seats,
        schedule.bookedSeats,
        schedule.fare)
      SchedulesTable += row
      schedule
    }

  private def buildSchedule(row: SchedulesRow, route: Route): Schedule =
    Schedule(
      row.sid,
      route,
      row.departDate,
      row.status,
      row.seats,
      row.bookedSeats,
      row.fare
    )


}


