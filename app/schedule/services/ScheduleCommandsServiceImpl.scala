package schedule.services

import javax.inject.Inject

import schedule.model.{Schedule, ScheduleStatus}
import schedule.persistence.ScheduleDAO
import schedule.route.model.Route
import sharedkernel.airport.model.Airport

import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent.Future

class ScheduleCommandsServiceImpl @Inject()(scheduleDAO: ScheduleDAO) extends ScheduleCommandsService{

  def updateScheduleSeatsAndStatus(scheduleId: Int, bookedSeats: Int, status: ScheduleStatus.Value): Future[Int] =
    Future.successful(scheduleDAO.updateScheduleSeatsAndStatus(scheduleId, bookedSeats, status))

  /** Used to create mock schedules */

  def create(schedule: Schedule): Future[Schedule] =
    Future.successful(scheduleDAO.create(schedule))

}
