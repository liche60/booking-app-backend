package schedule.services

import schedule.model.{Schedule, ScheduleStatus}
import schedule.route.model.Route
import sharedkernel.airport.model.Airport

import scala.concurrent.Future

trait ScheduleCommandsService {

  def updateScheduleSeatsAndStatus(scheduleId: Int, bookedSeats: Int, status: ScheduleStatus.Value): Future[Int]

  /** Used to create mock schedules */

  def create(schedule: Schedule): Future[Schedule]

}
