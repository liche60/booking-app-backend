package schedule.route.model

import sharedkernel.airline.model.Airline
import sharedkernel.airport.model.Airport

case class Route (
  rid: Int,
  airline: Airline,
  srcAirport: Airport,
  dstAirport: Airport,
  codeShare: Option[String],
  stops: Option[String],
  equipment: Option[String],
  added: Option[String]
)
