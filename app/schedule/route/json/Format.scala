package schedule.route.json

import play.api.libs.json._
import schedule.route.model.Route

object Format {

    import sharedkernel.airline.json.Format._
    import sharedkernel.airport.json.Format._

    implicit val routeWrites = Json.writes[Route]
    implicit val routeReads = Json.reads[Route]
}
