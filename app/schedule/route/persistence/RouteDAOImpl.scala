package schedule.route.persistence

import javax.inject.Inject

import play.Logger
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick._
import sharedkernel.airline.persistence.AirlineDAO
import sharedkernel.airport.persistence.AirportDAO
import sharedkernel.common.persistence.Tables.{RoutesRow, RoutesTable}

import schedule.route.model.Route
import sharedkernel.airport.model.Airport

/**
 * Give access to the user object using Slick
 */
class RouteDAOImpl  @Inject()(airlineDAO: AirlineDAO, airportDAO: AirportDAO) extends RouteDAO {

  import play.api.Play.current

  def find(rid: Int): Option[Route] =
    DB withSession { implicit session =>
      buildRoute(RoutesTable
        .filter(row => row.rid === rid)
        .firstOption
      )
    }

  def find(src: Airport, dst: Airport): List[Route] =
    DB withSession { implicit session =>
      RoutesTable
        .filter(row => row.srcApid === src.apid && row.dstApid === dst.apid)
        .mapResult(row => buildRoute(row))
        .list
    }

  def list(): List[Route] =
    DB withSession { implicit session =>
      RoutesTable.list.map(buildRoute(_))
    }

  private def buildRoute(_row: Option[RoutesRow]): Option[Route] = {
    _row match {
      case Some(row) =>
        val airline = airlineDAO.find(row.alid.get)
        val srcAirport = airportDAO.find(row.srcApid.get)
        val dstAirport = airportDAO.find(row.dstApid.get)
        Some(Route(
          row.rid,
          airline.get,
          srcAirport.get,
          dstAirport.get,
          row.codeshare,
          row.stops,
          row.equipment: Option[String],
          row.added
        ))
      case _ => None
    }
  }

  private def buildRoute(row: RoutesRow): Route = {
    val airline = airlineDAO.find(row.alid.get)
    val srcAirport = airportDAO.find(row.srcApid.get)
    val dstAirport = airportDAO.find(row.dstApid.get)
    Route(
      row.rid,
      airline.get,
      srcAirport.getOrElse(Airport.default),
      dstAirport.getOrElse(Airport.default),
      row.codeshare,
      row.stops,
      row.equipment: Option[String],
      row.added
    )
  }

}
