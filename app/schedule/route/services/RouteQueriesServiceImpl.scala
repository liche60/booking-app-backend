package schedule.route.services

import javax.inject.Inject

import schedule.route.model.Route
import schedule.route.persistence.RouteDAO
import sharedkernel.airport.model.Airport

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

class RouteQueriesServiceImpl @Inject()(routeDAO: RouteDAO) extends RouteQueriesService {

  def find(rid: Int): Future[Option[Route]] = Future.successful(routeDAO.find(rid))

  def find(src: Airport, dst: Airport): Future[List[Route]] = Future.successful(routeDAO.find(src, dst))

  def list(): Future[List[Route]] = Future.successful(routeDAO.list())

}
