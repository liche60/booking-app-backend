package schedule.model

import org.joda.time.DateTime
import schedule.route.model.Route

case class Schedule(
  sid: Int,
  route: Route,
  departDate: DateTime,
  status: ScheduleStatus.Value,
  seats: Int,
  bookedSeats: Int,
  fare: Long
)


