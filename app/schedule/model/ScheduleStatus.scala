package schedule.model

object ScheduleStatus extends Enumeration {
  val Opened, Closed, Flown = Value
}
