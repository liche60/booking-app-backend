package schedule.json

import org.joda.time.DateTime
import sharedkernel.airport.model.Airport

case class ScheduleBasicQuery(
  srcAirport: Airport,
  dstAirport: Airport,
  departDate: Option[DateTime],
  orderByFare: Boolean
)
