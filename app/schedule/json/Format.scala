package schedule.json

import play.api.libs.json._
import schedule.model.{Schedule, ScheduleStatus}
import sharedkernel.common.json.Format.enumFormat

object Format {

    import sharedkernel.airline.json.Format._
    import sharedkernel.airport.json.Format._

    import schedule.route.json.Format._
    import sharedkernel.common.json.Format.datetimeFormatWithHours

    implicit val scheduleStatusFormat: Format[ScheduleStatus.Value] = enumFormat( ScheduleStatus )

    implicit val scheduleWrites = Json.writes[Schedule]
    implicit val scheduleReads = Json.reads[Schedule]

    implicit val scheduleBasicQueryWrites = Json.writes[ScheduleBasicQuery]
    implicit val scheduleBasicQueryReads = Json.reads[ScheduleBasicQuery]

}
