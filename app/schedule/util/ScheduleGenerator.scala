package schedule.util

import javax.inject.Inject

import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import org.joda.time.DateTime
import play.Logger
import play.api.{Application, Play, Plugin}
import schedule.route.services.RouteQueriesService

import scala.concurrent.{Await, Future}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import schedule.model.{Schedule, ScheduleStatus}
import schedule.route.model.Route
import schedule.services.{ScheduleCommandsService, ScheduleQueriesService}

import scala.concurrent.duration.Duration

class ScheduleGenerator @Inject()(
                                    routeQueriesService: RouteQueriesService,
                                    scheduleCommandsService: ScheduleCommandsService,
                                    scheduleQueriesService: ScheduleQueriesService) {

  def start(): Unit = {
    Logger.debug("Creating schedules...")
    val fut = for {
      routes <- routeQueriesService.list()
      schedules <- scheduleQueriesService.list
    } yield {
      Logger.debug("Staring schedule creator with " + routes.size + " routes and " + schedules.size + " schedules...")
      val random = scala.util.Random
      val size = routes.size
      routes.zipWithIndex.foreach { routeWithIndex =>
        val route = routeWithIndex._1
        val index = routeWithIndex._2
        Logger.debug(s"checking route $index/$size")
        // creates schedule records for now to 30 days
        (0 to 7).foreach { day =>
          val departDate: DateTime = new DateTime().withTime(random.nextInt(23), random.nextInt(59), 0, 0).plusDays(day)
          val schedule = schedules.filter { schedule =>
            schedule.route.rid == route.rid &&
              schedule.departDate.getYear == departDate.getYear &&
              schedule.departDate.getMonthOfYear == departDate.getMonthOfYear &&
              schedule.departDate.getDayOfYear == departDate.getDayOfYear
          }.headOption
          val scheduleProb = random.nextInt(100)
          if (scheduleProb > 50 && !schedule.isDefined) {
            val seats = 10 + random.nextInt(100)
            val bookedSeats = random.nextInt(seats)
            val status = bookedSeats match {
              case x if x == seats => ScheduleStatus.Closed
              case _ => ScheduleStatus.Opened
            }
            val fare = ((57 + random.nextInt(400)) * 1000).toLong
            scheduleCommandsService.create(Schedule(0, route, departDate, status, seats, bookedSeats, fare))
          }
        }
      }
    }
    Await.result(fut, Duration.Inf)
    Logger.debug("finish Creating schedules...")
  }
}

object ScheduleGenerator {
  def start(injector: com.google.inject.Injector): Unit =
    injector.getInstance(classOf[ScheduleGenerator]).start()
}