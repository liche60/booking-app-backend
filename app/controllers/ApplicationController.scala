package controllers

import play.api.mvc.{Action, AnyContent, Controller}

object ApplicationController extends Controller {
  def options(path:String): Action[AnyContent] = Action { Ok("")}
}
