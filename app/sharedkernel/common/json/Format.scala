package sharedkernel.common.json

import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import play.api.libs.json._


object Format {

  val format = "dd/MM/yyyy"
  val formatter: DateTimeFormatter = DateTimeFormat.forPattern(format)

  implicit val datetimeFormat: Format[DateTime] = new Format[DateTime] {

    override def writes(date: DateTime): JsValue = {
      JsString(formatter.print(date.getMillis))
    }

    override def reads(json: JsValue): JsResult[DateTime] = {
      json match {
        case JsString(str) =>
          try {
            JsSuccess(formatter.parseDateTime(str))
          } catch {
            case e: Exception =>
              JsError(e.getMessage)
          }
        case _ => JsError(s"Date must have date format (without hours): $format")
      }

    }
  }

  val formatHours = "dd/MM/yyyy hh:mm:ss"
  val formatterHours: DateTimeFormatter = DateTimeFormat.forPattern(formatHours)

  implicit val datetimeFormatWithHours: Format[DateTime] = new Format[DateTime] {

    override def writes(date: DateTime): JsValue = {
      JsString(formatterHours.print(date.getMillis))
    }

    override def reads(json: JsValue): JsResult[DateTime] = {
      json match {
        case JsString(str) =>
          try {
            JsSuccess(formatterHours.parseDateTime(str))
          } catch {
            case e: Exception =>
              JsError(e.getMessage)
          }
        case _ => JsError(s"Date must have date format (with hours) $formatHours")
      }

    }
  }

  def enumFormat[E<: Enumeration](enum: E): Format[E#Value] = new Format[E#Value] {

    override def writes(e: E#Value): JsValue = JsString(e.toString)

    override def reads(json: JsValue): JsResult[E#Value] = {
      json match {
        case JsString(str) =>
          try {
            JsSuccess(enum.withName(str))
          } catch {
            case e: Exception =>
              JsError(s"Enum format expects a string of: ${enum.values.mkString(",")}")
          }
        case _ => JsError(s"String expected")
      }
    }

  }

}
