package sharedkernel.common.persistence
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = scala.slick.driver.MySQLDriver
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: scala.slick.driver.JdbcProfile
  import profile.simple._
  
      import sharedkernel.common.persistence.TypeMappers._
      import com.github.tototoshi.slick.MySQLJodaSupport._
      import org.joda.time.DateTime
      import scala.slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import scala.slick.jdbc.{GetResult => GR}
  
  /** DDL for all tables. Call .create to execute. */
  lazy val ddl = AirlinesTable.ddl ++ AirportsTable.ddl ++ CountriesTable.ddl ++ LoginInfoTable.ddl ++ Oauth1InfoTable.ddl ++ Oauth2InfoTable.ddl ++ PasswordInfoTable.ddl ++ RoutesTable.ddl ++ SchedulesTable.ddl ++ UserLoginInfoTable.ddl ++ UsersTable.ddl
  
  /** Entity class storing rows of table AirlinesTable
   *  @param name Database column name DBType(TEXT), Length(65535,true), Default(None)
   *  @param iata Database column iata DBType(VARCHAR), Length(2,true), Default(None)
   *  @param icao Database column icao DBType(VARCHAR), Length(3,true), Default(None)
   *  @param callsign Database column callsign DBType(TEXT), Length(65535,true), Default(None)
   *  @param country Database column country DBType(TEXT), Length(65535,true), Default(None)
   *  @param alid Database column alid DBType(INT), AutoInc, PrimaryKey
   *  @param uid Database column uid DBType(INT), Default(None)
   *  @param alias Database column alias DBType(TEXT), Length(65535,true), Default(None)
   *  @param mode Database column mode DBType(CHAR), Length(1,false), Default(Some(F))
   *  @param active Database column active DBType(VARCHAR), Length(1,true), Default(Some(N)) */
  case class AirlinesRow(name: Option[String] = None, iata: Option[String] = None, icao: Option[String] = None, callsign: Option[String] = None, country: Option[String] = None, alid: Int, uid: Option[Int] = None, alias: Option[String] = None, mode: Option[String] = Some("F"), active: Option[String] = Some("N"))
  /** GetResult implicit for fetching AirlinesRow objects using plain SQL queries */
  implicit def GetResultAirlinesRow(implicit e0: GR[Option[String]], e1: GR[Int], e2: GR[Option[Int]]): GR[AirlinesRow] = GR{
    prs => import prs._
    AirlinesRow.tupled((<<?[String], <<?[String], <<?[String], <<?[String], <<?[String], <<[Int], <<?[Int], <<?[String], <<?[String], <<?[String]))
  }
  /** Table description of table airlines. Objects of this class serve as prototypes for rows in queries. */
  class AirlinesTable(_tableTag: Tag) extends Table[AirlinesRow](_tableTag, "airlines") {
    def * = (name, iata, icao, callsign, country, alid, uid, alias, mode, active) <> (AirlinesRow.tupled, AirlinesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (name, iata, icao, callsign, country, alid.?, uid, alias, mode, active).shaped.<>({r=>import r._; _6.map(_=> AirlinesRow.tupled((_1, _2, _3, _4, _5, _6.get, _7, _8, _9, _10)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column name DBType(TEXT), Length(65535,true), Default(None) */
    val name: Column[Option[String]] = column[Option[String]]("name", O.Length(65535,varying=true), O.Default(None))
    /** Database column iata DBType(VARCHAR), Length(2,true), Default(None) */
    val iata: Column[Option[String]] = column[Option[String]]("iata", O.Length(2,varying=true), O.Default(None))
    /** Database column icao DBType(VARCHAR), Length(3,true), Default(None) */
    val icao: Column[Option[String]] = column[Option[String]]("icao", O.Length(3,varying=true), O.Default(None))
    /** Database column callsign DBType(TEXT), Length(65535,true), Default(None) */
    val callsign: Column[Option[String]] = column[Option[String]]("callsign", O.Length(65535,varying=true), O.Default(None))
    /** Database column country DBType(TEXT), Length(65535,true), Default(None) */
    val country: Column[Option[String]] = column[Option[String]]("country", O.Length(65535,varying=true), O.Default(None))
    /** Database column alid DBType(INT), AutoInc, PrimaryKey */
    val alid: Column[Int] = column[Int]("alid", O.AutoInc, O.PrimaryKey)
    /** Database column uid DBType(INT), Default(None) */
    val uid: Column[Option[Int]] = column[Option[Int]]("uid", O.Default(None))
    /** Database column alias DBType(TEXT), Length(65535,true), Default(None) */
    val alias: Column[Option[String]] = column[Option[String]]("alias", O.Length(65535,varying=true), O.Default(None))
    /** Database column mode DBType(CHAR), Length(1,false), Default(Some(F)) */
    val mode: Column[Option[String]] = column[Option[String]]("mode", O.Length(1,varying=false), O.Default(Some("F")))
    /** Database column active DBType(VARCHAR), Length(1,true), Default(Some(N)) */
    val active: Column[Option[String]] = column[Option[String]]("active", O.Length(1,varying=true), O.Default(Some("N")))
    
    /** Index over (iata) (database name iata) */
    val index1 = index("iata", iata)
    /** Index over (icao) (database name icao) */
    val index2 = index("icao", icao)
  }
  /** Collection-like TableQuery object for table AirlinesTable */
  lazy val AirlinesTable = new TableQuery(tag => new AirlinesTable(tag))
  
  /** Entity class storing rows of table AirportsTable
   *  @param name Database column name DBType(TEXT), Length(65535,true)
   *  @param city Database column city DBType(TEXT), Length(65535,true), Default(None)
   *  @param country Database column country DBType(TEXT), Length(65535,true), Default(None)
   *  @param iata Database column iata DBType(VARCHAR), Length(3,true), Default(None)
   *  @param icao Database column icao DBType(VARCHAR), Length(4,true), Default(None)
   *  @param x Database column x DBType(DOUBLE)
   *  @param y Database column y DBType(DOUBLE)
   *  @param elevation Database column elevation DBType(INT), Default(None)
   *  @param apid Database column apid DBType(INT), AutoInc, PrimaryKey
   *  @param uid Database column uid DBType(INT), Default(None)
   *  @param timezone Database column timezone DBType(FLOAT), Default(None)
   *  @param dst Database column dst DBType(CHAR), Length(1,false), Default(None) */
  case class AirportsRow(name: String, city: Option[String] = None, country: Option[String] = None, iata: Option[String] = None, icao: Option[String] = None, x: Double, y: Double, elevation: Option[Int] = None, apid: Int, uid: Option[Int] = None, timezone: Option[Float] = None, dst: Option[String] = None)
  /** GetResult implicit for fetching AirportsRow objects using plain SQL queries */
  implicit def GetResultAirportsRow(implicit e0: GR[String], e1: GR[Option[String]], e2: GR[Double], e3: GR[Option[Int]], e4: GR[Int], e5: GR[Option[Float]]): GR[AirportsRow] = GR{
    prs => import prs._
    AirportsRow.tupled((<<[String], <<?[String], <<?[String], <<?[String], <<?[String], <<[Double], <<[Double], <<?[Int], <<[Int], <<?[Int], <<?[Float], <<?[String]))
  }
  /** Table description of table airports. Objects of this class serve as prototypes for rows in queries. */
  class AirportsTable(_tableTag: Tag) extends Table[AirportsRow](_tableTag, "airports") {
    def * = (name, city, country, iata, icao, x, y, elevation, apid, uid, timezone, dst) <> (AirportsRow.tupled, AirportsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (name.?, city, country, iata, icao, x.?, y.?, elevation, apid.?, uid, timezone, dst).shaped.<>({r=>import r._; _1.map(_=> AirportsRow.tupled((_1.get, _2, _3, _4, _5, _6.get, _7.get, _8, _9.get, _10, _11, _12)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column name DBType(TEXT), Length(65535,true) */
    val name: Column[String] = column[String]("name", O.Length(65535,varying=true))
    /** Database column city DBType(TEXT), Length(65535,true), Default(None) */
    val city: Column[Option[String]] = column[Option[String]]("city", O.Length(65535,varying=true), O.Default(None))
    /** Database column country DBType(TEXT), Length(65535,true), Default(None) */
    val country: Column[Option[String]] = column[Option[String]]("country", O.Length(65535,varying=true), O.Default(None))
    /** Database column iata DBType(VARCHAR), Length(3,true), Default(None) */
    val iata: Column[Option[String]] = column[Option[String]]("iata", O.Length(3,varying=true), O.Default(None))
    /** Database column icao DBType(VARCHAR), Length(4,true), Default(None) */
    val icao: Column[Option[String]] = column[Option[String]]("icao", O.Length(4,varying=true), O.Default(None))
    /** Database column x DBType(DOUBLE) */
    val x: Column[Double] = column[Double]("x")
    /** Database column y DBType(DOUBLE) */
    val y: Column[Double] = column[Double]("y")
    /** Database column elevation DBType(INT), Default(None) */
    val elevation: Column[Option[Int]] = column[Option[Int]]("elevation", O.Default(None))
    /** Database column apid DBType(INT), AutoInc, PrimaryKey */
    val apid: Column[Int] = column[Int]("apid", O.AutoInc, O.PrimaryKey)
    /** Database column uid DBType(INT), Default(None) */
    val uid: Column[Option[Int]] = column[Option[Int]]("uid", O.Default(None))
    /** Database column timezone DBType(FLOAT), Default(None) */
    val timezone: Column[Option[Float]] = column[Option[Float]]("timezone", O.Default(None))
    /** Database column dst DBType(CHAR), Length(1,false), Default(None) */
    val dst: Column[Option[String]] = column[Option[String]]("dst", O.Length(1,varying=false), O.Default(None))
    
    /** Index over (iata) (database name iata) */
    val index1 = index("iata", iata)
    /** Uniqueness Index over (iata) (database name iata_idx) */
    val index2 = index("iata_idx", iata, unique=true)
    /** Uniqueness Index over (icao) (database name icao_idx) */
    val index3 = index("icao_idx", icao, unique=true)
    /** Index over (x) (database name x) */
    val index4 = index("x", x)
    /** Index over (y) (database name y) */
    val index5 = index("y", y)
  }
  /** Collection-like TableQuery object for table AirportsTable */
  lazy val AirportsTable = new TableQuery(tag => new AirportsTable(tag))
  
  /** Entity class storing rows of table CountriesTable
   *  @param junk Database column junk DBType(TEXT), Length(65535,true), Default(None)
   *  @param code Database column code DBType(VARCHAR), PrimaryKey, Length(2,true)
   *  @param name Database column name DBType(TEXT), Length(65535,true), Default(None)
   *  @param oaCode Database column oa_code DBType(VARCHAR), Length(2,true), Default(None)
   *  @param dst Database column dst DBType(CHAR), Length(1,false), Default(None) */
  case class CountriesRow(junk: Option[String] = None, code: String, name: Option[String] = None, oaCode: Option[String] = None, dst: Option[String] = None)
  /** GetResult implicit for fetching CountriesRow objects using plain SQL queries */
  implicit def GetResultCountriesRow(implicit e0: GR[Option[String]], e1: GR[String]): GR[CountriesRow] = GR{
    prs => import prs._
    CountriesRow.tupled((<<?[String], <<[String], <<?[String], <<?[String], <<?[String]))
  }
  /** Table description of table countries. Objects of this class serve as prototypes for rows in queries. */
  class CountriesTable(_tableTag: Tag) extends Table[CountriesRow](_tableTag, "countries") {
    def * = (junk, code, name, oaCode, dst) <> (CountriesRow.tupled, CountriesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (junk, code.?, name, oaCode, dst).shaped.<>({r=>import r._; _2.map(_=> CountriesRow.tupled((_1, _2.get, _3, _4, _5)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column junk DBType(TEXT), Length(65535,true), Default(None) */
    val junk: Column[Option[String]] = column[Option[String]]("junk", O.Length(65535,varying=true), O.Default(None))
    /** Database column code DBType(VARCHAR), PrimaryKey, Length(2,true) */
    val code: Column[String] = column[String]("code", O.PrimaryKey, O.Length(2,varying=true))
    /** Database column name DBType(TEXT), Length(65535,true), Default(None) */
    val name: Column[Option[String]] = column[Option[String]]("name", O.Length(65535,varying=true), O.Default(None))
    /** Database column oa_code DBType(VARCHAR), Length(2,true), Default(None) */
    val oaCode: Column[Option[String]] = column[Option[String]]("oa_code", O.Length(2,varying=true), O.Default(None))
    /** Database column dst DBType(CHAR), Length(1,false), Default(None) */
    val dst: Column[Option[String]] = column[Option[String]]("dst", O.Length(1,varying=false), O.Default(None))
  }
  /** Collection-like TableQuery object for table CountriesTable */
  lazy val CountriesTable = new TableQuery(tag => new CountriesTable(tag))
  
  /** Entity class storing rows of table LoginInfoTable
   *  @param id Database column id DBType(BIGINT), AutoInc, PrimaryKey
   *  @param providerId Database column provider_id DBType(VARCHAR), Length(254,true)
   *  @param providerKey Database column provider_key DBType(VARCHAR), Length(254,true) */
  case class LoginInfoRow(id: Long, providerId: String, providerKey: String)
  /** GetResult implicit for fetching LoginInfoRow objects using plain SQL queries */
  implicit def GetResultLoginInfoRow(implicit e0: GR[Long], e1: GR[String]): GR[LoginInfoRow] = GR{
    prs => import prs._
    LoginInfoRow.tupled((<<[Long], <<[String], <<[String]))
  }
  /** Table description of table login_info. Objects of this class serve as prototypes for rows in queries. */
  class LoginInfoTable(_tableTag: Tag) extends Table[LoginInfoRow](_tableTag, "login_info") {
    def * = (id, providerId, providerKey) <> (LoginInfoRow.tupled, LoginInfoRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (id.?, providerId.?, providerKey.?).shaped.<>({r=>import r._; _1.map(_=> LoginInfoRow.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column id DBType(BIGINT), AutoInc, PrimaryKey */
    val id: Column[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    /** Database column provider_id DBType(VARCHAR), Length(254,true) */
    val providerId: Column[String] = column[String]("provider_id", O.Length(254,varying=true))
    /** Database column provider_key DBType(VARCHAR), Length(254,true) */
    val providerKey: Column[String] = column[String]("provider_key", O.Length(254,varying=true))
  }
  /** Collection-like TableQuery object for table LoginInfoTable */
  lazy val LoginInfoTable = new TableQuery(tag => new LoginInfoTable(tag))
  
  /** Entity class storing rows of table Oauth1InfoTable
   *  @param id Database column id DBType(BIGINT), AutoInc, PrimaryKey
   *  @param token Database column token DBType(VARCHAR), Length(254,true)
   *  @param secret Database column secret DBType(VARCHAR), Length(254,true)
   *  @param loginInfoId Database column login_info_id DBType(BIGINT) */
  case class Oauth1InfoRow(id: Long, token: String, secret: String, loginInfoId: Long)
  /** GetResult implicit for fetching Oauth1InfoRow objects using plain SQL queries */
  implicit def GetResultOauth1InfoRow(implicit e0: GR[Long], e1: GR[String]): GR[Oauth1InfoRow] = GR{
    prs => import prs._
    Oauth1InfoRow.tupled((<<[Long], <<[String], <<[String], <<[Long]))
  }
  /** Table description of table oauth1_info. Objects of this class serve as prototypes for rows in queries. */
  class Oauth1InfoTable(_tableTag: Tag) extends Table[Oauth1InfoRow](_tableTag, "oauth1_info") {
    def * = (id, token, secret, loginInfoId) <> (Oauth1InfoRow.tupled, Oauth1InfoRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (id.?, token.?, secret.?, loginInfoId.?).shaped.<>({r=>import r._; _1.map(_=> Oauth1InfoRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column id DBType(BIGINT), AutoInc, PrimaryKey */
    val id: Column[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    /** Database column token DBType(VARCHAR), Length(254,true) */
    val token: Column[String] = column[String]("token", O.Length(254,varying=true))
    /** Database column secret DBType(VARCHAR), Length(254,true) */
    val secret: Column[String] = column[String]("secret", O.Length(254,varying=true))
    /** Database column login_info_id DBType(BIGINT) */
    val loginInfoId: Column[Long] = column[Long]("login_info_id")
  }
  /** Collection-like TableQuery object for table Oauth1InfoTable */
  lazy val Oauth1InfoTable = new TableQuery(tag => new Oauth1InfoTable(tag))
  
  /** Entity class storing rows of table Oauth2InfoTable
   *  @param id Database column id DBType(BIGINT), AutoInc, PrimaryKey
   *  @param accessToken Database column access_token DBType(VARCHAR), Length(254,true)
   *  @param tokenType Database column token_type DBType(VARCHAR), Length(254,true), Default(None)
   *  @param expiresIn Database column expires_in DBType(INT), Default(None)
   *  @param refreshToken Database column refresh_token DBType(VARCHAR), Length(254,true), Default(None)
   *  @param loginInfoId Database column login_info_id DBType(BIGINT) */
  case class Oauth2InfoRow(id: Long, accessToken: String, tokenType: Option[String] = None, expiresIn: Option[Int] = None, refreshToken: Option[String] = None, loginInfoId: Long)
  /** GetResult implicit for fetching Oauth2InfoRow objects using plain SQL queries */
  implicit def GetResultOauth2InfoRow(implicit e0: GR[Long], e1: GR[String], e2: GR[Option[String]], e3: GR[Option[Int]]): GR[Oauth2InfoRow] = GR{
    prs => import prs._
    Oauth2InfoRow.tupled((<<[Long], <<[String], <<?[String], <<?[Int], <<?[String], <<[Long]))
  }
  /** Table description of table oauth2_info. Objects of this class serve as prototypes for rows in queries. */
  class Oauth2InfoTable(_tableTag: Tag) extends Table[Oauth2InfoRow](_tableTag, "oauth2_info") {
    def * = (id, accessToken, tokenType, expiresIn, refreshToken, loginInfoId) <> (Oauth2InfoRow.tupled, Oauth2InfoRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (id.?, accessToken.?, tokenType, expiresIn, refreshToken, loginInfoId.?).shaped.<>({r=>import r._; _1.map(_=> Oauth2InfoRow.tupled((_1.get, _2.get, _3, _4, _5, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column id DBType(BIGINT), AutoInc, PrimaryKey */
    val id: Column[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    /** Database column access_token DBType(VARCHAR), Length(254,true) */
    val accessToken: Column[String] = column[String]("access_token", O.Length(254,varying=true))
    /** Database column token_type DBType(VARCHAR), Length(254,true), Default(None) */
    val tokenType: Column[Option[String]] = column[Option[String]]("token_type", O.Length(254,varying=true), O.Default(None))
    /** Database column expires_in DBType(INT), Default(None) */
    val expiresIn: Column[Option[Int]] = column[Option[Int]]("expires_in", O.Default(None))
    /** Database column refresh_token DBType(VARCHAR), Length(254,true), Default(None) */
    val refreshToken: Column[Option[String]] = column[Option[String]]("refresh_token", O.Length(254,varying=true), O.Default(None))
    /** Database column login_info_id DBType(BIGINT) */
    val loginInfoId: Column[Long] = column[Long]("login_info_id")
  }
  /** Collection-like TableQuery object for table Oauth2InfoTable */
  lazy val Oauth2InfoTable = new TableQuery(tag => new Oauth2InfoTable(tag))
  
  /** Entity class storing rows of table PasswordInfoTable
   *  @param hasher Database column hasher DBType(VARCHAR), Length(254,true)
   *  @param password Database column password DBType(VARCHAR), Length(254,true)
   *  @param salt Database column salt DBType(VARCHAR), Length(254,true), Default(None)
   *  @param loginInfoId Database column login_info_id DBType(BIGINT) */
  case class PasswordInfoRow(hasher: String, password: String, salt: Option[String] = None, loginInfoId: Long)
  /** GetResult implicit for fetching PasswordInfoRow objects using plain SQL queries */
  implicit def GetResultPasswordInfoRow(implicit e0: GR[String], e1: GR[Option[String]], e2: GR[Long]): GR[PasswordInfoRow] = GR{
    prs => import prs._
    PasswordInfoRow.tupled((<<[String], <<[String], <<?[String], <<[Long]))
  }
  /** Table description of table password_info. Objects of this class serve as prototypes for rows in queries. */
  class PasswordInfoTable(_tableTag: Tag) extends Table[PasswordInfoRow](_tableTag, "password_info") {
    def * = (hasher, password, salt, loginInfoId) <> (PasswordInfoRow.tupled, PasswordInfoRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (hasher.?, password.?, salt, loginInfoId.?).shaped.<>({r=>import r._; _1.map(_=> PasswordInfoRow.tupled((_1.get, _2.get, _3, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column hasher DBType(VARCHAR), Length(254,true) */
    val hasher: Column[String] = column[String]("hasher", O.Length(254,varying=true))
    /** Database column password DBType(VARCHAR), Length(254,true) */
    val password: Column[String] = column[String]("password", O.Length(254,varying=true))
    /** Database column salt DBType(VARCHAR), Length(254,true), Default(None) */
    val salt: Column[Option[String]] = column[Option[String]]("salt", O.Length(254,varying=true), O.Default(None))
    /** Database column login_info_id DBType(BIGINT) */
    val loginInfoId: Column[Long] = column[Long]("login_info_id")
  }
  /** Collection-like TableQuery object for table PasswordInfoTable */
  lazy val PasswordInfoTable = new TableQuery(tag => new PasswordInfoTable(tag))
  
  /** Entity class storing rows of table RoutesTable
   *  @param airline Database column airline DBType(VARCHAR), Length(3,true), Default(None)
   *  @param alid Database column alid DBType(INT), Default(None)
   *  @param srcAp Database column src_ap DBType(VARCHAR), Length(4,true), Default(None)
   *  @param srcApid Database column src_apid DBType(INT), Default(None)
   *  @param dstAp Database column dst_ap DBType(VARCHAR), Length(4,true), Default(None)
   *  @param dstApid Database column dst_apid DBType(INT), Default(None)
   *  @param codeshare Database column codeshare DBType(TEXT), Length(65535,true), Default(None)
   *  @param stops Database column stops DBType(TEXT), Length(65535,true), Default(None)
   *  @param equipment Database column equipment DBType(TEXT), Length(65535,true), Default(None)
   *  @param added Database column added DBType(VARCHAR), Length(1,true), Default(None)
   *  @param rid Database column rid DBType(INT), AutoInc, PrimaryKey */
  case class RoutesRow(airline: Option[String] = None, alid: Option[Int] = None, srcAp: Option[String] = None, srcApid: Option[Int] = None, dstAp: Option[String] = None, dstApid: Option[Int] = None, codeshare: Option[String] = None, stops: Option[String] = None, equipment: Option[String] = None, added: Option[String] = None, rid: Int)
  /** GetResult implicit for fetching RoutesRow objects using plain SQL queries */
  implicit def GetResultRoutesRow(implicit e0: GR[Option[String]], e1: GR[Option[Int]], e2: GR[Int]): GR[RoutesRow] = GR{
    prs => import prs._
    RoutesRow.tupled((<<?[String], <<?[Int], <<?[String], <<?[Int], <<?[String], <<?[Int], <<?[String], <<?[String], <<?[String], <<?[String], <<[Int]))
  }
  /** Table description of table routes. Objects of this class serve as prototypes for rows in queries. */
  class RoutesTable(_tableTag: Tag) extends Table[RoutesRow](_tableTag, "routes") {
    def * = (airline, alid, srcAp, srcApid, dstAp, dstApid, codeshare, stops, equipment, added, rid) <> (RoutesRow.tupled, RoutesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (airline, alid, srcAp, srcApid, dstAp, dstApid, codeshare, stops, equipment, added, rid.?).shaped.<>({r=>import r._; _11.map(_=> RoutesRow.tupled((_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column airline DBType(VARCHAR), Length(3,true), Default(None) */
    val airline: Column[Option[String]] = column[Option[String]]("airline", O.Length(3,varying=true), O.Default(None))
    /** Database column alid DBType(INT), Default(None) */
    val alid: Column[Option[Int]] = column[Option[Int]]("alid", O.Default(None))
    /** Database column src_ap DBType(VARCHAR), Length(4,true), Default(None) */
    val srcAp: Column[Option[String]] = column[Option[String]]("src_ap", O.Length(4,varying=true), O.Default(None))
    /** Database column src_apid DBType(INT), Default(None) */
    val srcApid: Column[Option[Int]] = column[Option[Int]]("src_apid", O.Default(None))
    /** Database column dst_ap DBType(VARCHAR), Length(4,true), Default(None) */
    val dstAp: Column[Option[String]] = column[Option[String]]("dst_ap", O.Length(4,varying=true), O.Default(None))
    /** Database column dst_apid DBType(INT), Default(None) */
    val dstApid: Column[Option[Int]] = column[Option[Int]]("dst_apid", O.Default(None))
    /** Database column codeshare DBType(TEXT), Length(65535,true), Default(None) */
    val codeshare: Column[Option[String]] = column[Option[String]]("codeshare", O.Length(65535,varying=true), O.Default(None))
    /** Database column stops DBType(TEXT), Length(65535,true), Default(None) */
    val stops: Column[Option[String]] = column[Option[String]]("stops", O.Length(65535,varying=true), O.Default(None))
    /** Database column equipment DBType(TEXT), Length(65535,true), Default(None) */
    val equipment: Column[Option[String]] = column[Option[String]]("equipment", O.Length(65535,varying=true), O.Default(None))
    /** Database column added DBType(VARCHAR), Length(1,true), Default(None) */
    val added: Column[Option[String]] = column[Option[String]]("added", O.Length(1,varying=true), O.Default(None))
    /** Database column rid DBType(INT), AutoInc, PrimaryKey */
    val rid: Column[Int] = column[Int]("rid", O.AutoInc, O.PrimaryKey)
    
    /** Uniqueness Index over (alid,srcApid,dstApid) (database name alid) */
    val index1 = index("alid", (alid, srcApid, dstApid), unique=true)
    /** Index over (dstApid) (database name dst_apid) */
    val index2 = index("dst_apid", dstApid)
    /** Index over (srcApid) (database name src_apid) */
    val index3 = index("src_apid", srcApid)
  }
  /** Collection-like TableQuery object for table RoutesTable */
  lazy val RoutesTable = new TableQuery(tag => new RoutesTable(tag))
  
  /** Entity class storing rows of table SchedulesTable
   *  @param sid Database column sid DBType(INT), AutoInc, PrimaryKey
   *  @param rid Database column rid DBType(INT)
   *  @param departDate Database column depart_date DBType(DATETIME)
   *  @param status Database column status DBType(VARCHAR), Length(50,true)
   *  @param seats Database column seats DBType(INT)
   *  @param bookedSeats Database column booked_seats DBType(INT)
   *  @param fare Database column fare DBType(BIGINT) */
  case class SchedulesRow(sid: Int, rid: Int, departDate: DateTime, status: schedule.model.ScheduleStatus.Value, seats: Int, bookedSeats: Int, fare: Long)
  /** GetResult implicit for fetching SchedulesRow objects using plain SQL queries */
  implicit def GetResultSchedulesRow(implicit e0: GR[Int], e1: GR[DateTime], e2: GR[schedule.model.ScheduleStatus.Value], e3: GR[Long]): GR[SchedulesRow] = GR{
    prs => import prs._
    SchedulesRow.tupled((<<[Int], <<[Int], <<[DateTime], <<[schedule.model.ScheduleStatus.Value], <<[Int], <<[Int], <<[Long]))
  }
  /** Table description of table schedules. Objects of this class serve as prototypes for rows in queries. */
  class SchedulesTable(_tableTag: Tag) extends Table[SchedulesRow](_tableTag, "schedules") {
    def * = (sid, rid, departDate, status, seats, bookedSeats, fare) <> (SchedulesRow.tupled, SchedulesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (sid.?, rid.?, departDate.?, status.?, seats.?, bookedSeats.?, fare.?).shaped.<>({r=>import r._; _1.map(_=> SchedulesRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column sid DBType(INT), AutoInc, PrimaryKey */
    val sid: Column[Int] = column[Int]("sid", O.AutoInc, O.PrimaryKey)
    /** Database column rid DBType(INT) */
    val rid: Column[Int] = column[Int]("rid")
    /** Database column depart_date DBType(DATETIME) */
    val departDate: Column[DateTime] = column[DateTime]("depart_date")
    /** Database column status DBType(VARCHAR), Length(50,true) */
    val status: Column[schedule.model.ScheduleStatus.Value] = column[schedule.model.ScheduleStatus.Value]("status", O.Length(50,varying=true))
    /** Database column seats DBType(INT) */
    val seats: Column[Int] = column[Int]("seats")
    /** Database column booked_seats DBType(INT) */
    val bookedSeats: Column[Int] = column[Int]("booked_seats")
    /** Database column fare DBType(BIGINT) */
    val fare: Column[Long] = column[Long]("fare")
    
    /** Foreign key referencing RoutesTable (database name fk_routes_rid) */
    lazy val routesTableFk = foreignKey("fk_routes_rid", rid, RoutesTable)(r => r.rid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table SchedulesTable */
  lazy val SchedulesTable = new TableQuery(tag => new SchedulesTable(tag))
  
  /** Entity class storing rows of table UserLoginInfoTable
   *  @param userId Database column user_id DBType(VARCHAR), Length(254,true)
   *  @param loginInfoId Database column login_info_id DBType(BIGINT) */
  case class UserLoginInfoRow(userId: String, loginInfoId: Long)
  /** GetResult implicit for fetching UserLoginInfoRow objects using plain SQL queries */
  implicit def GetResultUserLoginInfoRow(implicit e0: GR[String], e1: GR[Long]): GR[UserLoginInfoRow] = GR{
    prs => import prs._
    UserLoginInfoRow.tupled((<<[String], <<[Long]))
  }
  /** Table description of table user_login_info. Objects of this class serve as prototypes for rows in queries. */
  class UserLoginInfoTable(_tableTag: Tag) extends Table[UserLoginInfoRow](_tableTag, "user_login_info") {
    def * = (userId, loginInfoId) <> (UserLoginInfoRow.tupled, UserLoginInfoRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (userId.?, loginInfoId.?).shaped.<>({r=>import r._; _1.map(_=> UserLoginInfoRow.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column user_id DBType(VARCHAR), Length(254,true) */
    val userId: Column[String] = column[String]("user_id", O.Length(254,varying=true))
    /** Database column login_info_id DBType(BIGINT) */
    val loginInfoId: Column[Long] = column[Long]("login_info_id")
  }
  /** Collection-like TableQuery object for table UserLoginInfoTable */
  lazy val UserLoginInfoTable = new TableQuery(tag => new UserLoginInfoTable(tag))
  
  /** Entity class storing rows of table UsersTable
   *  @param id Database column id DBType(VARCHAR), PrimaryKey, Length(254,true)
   *  @param firstName Database column first_name DBType(VARCHAR), Length(254,true), Default(None)
   *  @param lastName Database column last_name DBType(VARCHAR), Length(254,true), Default(None)
   *  @param fullName Database column full_name DBType(VARCHAR), Length(254,true), Default(None)
   *  @param email Database column email DBType(VARCHAR), Length(254,true), Default(None)
   *  @param avatarUrl Database column avatar_url DBType(VARCHAR), Length(254,true), Default(None) */
  case class UsersRow(id: String, firstName: Option[String] = None, lastName: Option[String] = None, fullName: Option[String] = None, email: Option[String] = None, avatarUrl: Option[String] = None)
  /** GetResult implicit for fetching UsersRow objects using plain SQL queries */
  implicit def GetResultUsersRow(implicit e0: GR[String], e1: GR[Option[String]]): GR[UsersRow] = GR{
    prs => import prs._
    UsersRow.tupled((<<[String], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String]))
  }
  /** Table description of table users. Objects of this class serve as prototypes for rows in queries. */
  class UsersTable(_tableTag: Tag) extends Table[UsersRow](_tableTag, "users") {
    def * = (id, firstName, lastName, fullName, email, avatarUrl) <> (UsersRow.tupled, UsersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (id.?, firstName, lastName, fullName, email, avatarUrl).shaped.<>({r=>import r._; _1.map(_=> UsersRow.tupled((_1.get, _2, _3, _4, _5, _6)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column id DBType(VARCHAR), PrimaryKey, Length(254,true) */
    val id: Column[String] = column[String]("id", O.PrimaryKey, O.Length(254,varying=true))
    /** Database column first_name DBType(VARCHAR), Length(254,true), Default(None) */
    val firstName: Column[Option[String]] = column[Option[String]]("first_name", O.Length(254,varying=true), O.Default(None))
    /** Database column last_name DBType(VARCHAR), Length(254,true), Default(None) */
    val lastName: Column[Option[String]] = column[Option[String]]("last_name", O.Length(254,varying=true), O.Default(None))
    /** Database column full_name DBType(VARCHAR), Length(254,true), Default(None) */
    val fullName: Column[Option[String]] = column[Option[String]]("full_name", O.Length(254,varying=true), O.Default(None))
    /** Database column email DBType(VARCHAR), Length(254,true), Default(None) */
    val email: Column[Option[String]] = column[Option[String]]("email", O.Length(254,varying=true), O.Default(None))
    /** Database column avatar_url DBType(VARCHAR), Length(254,true), Default(None) */
    val avatarUrl: Column[Option[String]] = column[Option[String]]("avatar_url", O.Length(254,varying=true), O.Default(None))
  }
  /** Collection-like TableQuery object for table UsersTable */
  lazy val UsersTable = new TableQuery(tag => new UsersTable(tag))
}