package sharedkernel.common.persistence

import sharedkernel.common.persistence.Tables.profile.simple._
import schedule.model.ScheduleStatus

object TypeMappers {

  def EnumTypeMapper[E <: Enumeration](enum: E): BaseColumnType[enum.Value] = MappedColumnType.base[enum.Value, String](
    enumValue => enumValue.toString,
    str => enum.withName(str)
  )

  implicit val scheduleStatusEnumTypeMapper = EnumTypeMapper(ScheduleStatus)

}
