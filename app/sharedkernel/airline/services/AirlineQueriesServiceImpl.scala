package sharedkernel.airline.services

import javax.inject.Inject

import sharedkernel.airline.model.Airline
import sharedkernel.airline.persistence.AirlineDAO

import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent.Future

class AirlineQueriesServiceImpl @Inject()(airlineDAO: AirlineDAO) extends AirlineQueriesService {

  def find(aid: Int): Future[Option[Airline]] = Future.successful(airlineDAO.find(aid))

}
