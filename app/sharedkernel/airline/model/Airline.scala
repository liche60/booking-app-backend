package sharedkernel.airline.model

case class Airline(
  name: Option[String],
  iata: Option[String],
  icao: Option[String],
  callsign: Option[String],
  country: Option[String],
  alid: Int,
  uid: Option[Int],
  alias: Option[String],
  mode: Option[String],
  active: Option[String]
)
