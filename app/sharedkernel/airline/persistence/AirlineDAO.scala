package sharedkernel.airline.persistence

import sharedkernel.airline.model.Airline

import scala.concurrent.Future

trait AirlineDAO {

  def find(aid: Int): Option[Airline]

}
