package sharedkernel.airline.persistence

import play.api.db.slick.Config.driver.simple._
import play.api.db.slick._
import sharedkernel.common.persistence.Tables.{AirlinesTable}
import sharedkernel.airline.model.Airline

import play.api.Play.current

class AirlineDAOImpl extends AirlineDAO {

  def find(aid: Int): Option[Airline] =
    DB withSession { implicit session =>
      AirlinesTable
        .filter(row => row.alid === aid)
        .mapResult(row =>
          Airline(
            row.name,
            row.iata,
            row.icao,
            row.callsign,
            row.country,
            row.alid,
            row.uid,
            row.alias,
            row.mode,
            row.active
          )
      ).firstOption
    }

}
