package sharedkernel.airport.persistence

import sharedkernel.airport.model.Airport

trait AirportDAO {

  def list(airportQuery: Option[String]): List[Airport]

  def find(aid: Int): Option[Airport]

}
