Booking App - Backend
==============================

Api for Booking App, created with scala using:

Security:
Social Oauth: https://github.com/mohiva/play-silhouette

Database: this app use models from OpenFlight: http://openflights.org/data.html

Database: MySql

Database connection: Slick (https://github.com/playframework/play-slick)

Injector: Google Guice

Message/Events: Akka


## Architecture 

Booking App, was designed to expose a reactive microservice architecture. 
This business case, was designed using DDD:

-> shared kernel domain: this bounded context contains common components as airports, airlines

-> secutity: Allows social oauth autenticacion, and CORS filter

-> user: this bounded context contains services to retrieve and create users

-> shcedule: this bounded context allows queries to find available flights. It also contains the routes sub-domain

-> booking: realize the goal of the problem allowing users to book scheduled routes 

## Config

change database connection settings in config/application.conf:
 
    db.default.driver=com.mysql.jdbc.Driver
    db.default.url="jdbc:mysql://localhost/schema"
    db.default.user=your_db_user
    db.default.password="your_db_password"

This app use db evolutions, so it requires only a empty schema and DDL´s user. 

# Running

Use sbt (http://www.scala-sbt.org/) to compile/run the project:

sbt compile

sbt run

Happy coding!