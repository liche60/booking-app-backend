# --- DDL to schedule domain

# --- !Ups

CREATE TABLE `schedules` (
  `sid` INT NOT NULL AUTO_INCREMENT,
  `rid` INT(11) NOT NULL,
  `depart_date` DATETIME NOT NULL,
  `status` VARCHAR(50) NOT NULL COMMENT 'schedule status (Opened, Closed, Flown)',
  `seats` INT NOT NULL,
  `booked_seats` INT NOT NULL,
  `fare` BIGINT NOT NULL,
  PRIMARY KEY (`sid`),
  INDEX `fk_routes_rid_idx` (`rid` ASC),
  CONSTRAINT `fk_routes_rid`
    FOREIGN KEY (`rid`)
    REFERENCES `booking-app`.`routes` (`rid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


# --- !Downs
drop table `schedules`;