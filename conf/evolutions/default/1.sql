# --- DDL to user and security domain

# --- !Ups

# --- Required for change play evolutions from 64kb to 4gb
ALTER TABLE `play_evolutions` MODIFY COLUMN `apply_script` LONGTEXT;

create table `login_info` (`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,`provider_id` VARCHAR(254) NOT NULL,`provider_key` VARCHAR(254) NOT NULL);
create table `oauth1_info` (`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,`token` VARCHAR(254) NOT NULL,`secret` VARCHAR(254) NOT NULL,`login_info_id` BIGINT NOT NULL);
create table `oauth2_info` (`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,`access_token` VARCHAR(254) NOT NULL,`token_type` VARCHAR(254),`expires_in` INTEGER,`refresh_token` VARCHAR(254),`login_info_id` BIGINT NOT NULL);
create table `password_info` (`hasher` VARCHAR(254) NOT NULL,`password` VARCHAR(254) NOT NULL,`salt` VARCHAR(254),`login_info_id` BIGINT NOT NULL);
create table `users` (`id` VARCHAR(254) NOT NULL PRIMARY KEY,`first_name` VARCHAR(254),`last_name` VARCHAR(254),`full_name` VARCHAR(254),`email` VARCHAR(254),`avatar_url` VARCHAR(254));
create table `user_login_info` (`user_id` VARCHAR(254) NOT NULL,`login_info_id` BIGINT NOT NULL);

# --- !Downs

drop table `user_login_info`;
drop table `users`;
drop table `password_info`;
drop table `oauth2_info`;
drop table `oauth1_info`;
drop table `login_info`;

